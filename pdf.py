from importlib.resources import path
import os
import textwrap
from tkinter import *
from tkinter import filedialog, Text
from tkinter import messagebox
from PIL import Image, ImageTk
from PyPDF2 import *
from datetime import datetime

files = []
i=0
saved=True
filename=""
path=""


class DragDropListbox(Listbox):
    def __init__(self, master, **kw):
        kw['selectmode'] = SINGLE
        Listbox.__init__(self, master, kw, selectbackground="red", selectforeground="white", font=("Helvetica",9,"bold"))
        self.bind('<Button-1>', self.setCurrent)
        self.bind('<B1-Motion>', self.shiftSelection)
        self.curIndex = None

    def setCurrent(self, event):
        self.curIndex = self.nearest(event.y)

    def shiftSelection(self, event):
        i = self.nearest(event.y)
        if i < self.curIndex:
            x = self.get(i)
            self.delete(i)
            self.insert(i+1, x)
            self.curIndex = i
        elif i > self.curIndex:
            x = self.get(i)
            self.delete(i)
            self.insert(i-1, x)
            self.curIndex = i

class Table:
    entries = []
    def __init__(self,root):    
        root.columnconfigure(0,weight=1)
        root.columnconfigure(1,weight=1)
        for j in range(1,11):     
            self.e = Entry(root)
            self.e.grid(row=j, column=0, sticky='EW')
            self.e2 = Entry(root)
            self.e2.grid(row=j, column=1, sticky='EW')
            self.entries.append((self.e,self.e2))

def merge_pdfs(path):
    selection = ui.get(0,END)
    merger = PdfMerger()
    problems = ""
    for item in selection:
        if os.path.isfile(str(item).strip()):
            merger.append(item)
        else:
            problems+=(item+'\n')
    merger.write(path)
    merger.close()
    if len(problems) > 0 :
        messagebox.showwarning("showwarning", "These File(s) could not be merged due to some problems:\n"+problems)

def create_file_name(fname):
    curr = datetime.now()
    return fname+'_'+str(curr.year)+'_'+str(curr.month)+'_'+str(curr.day)+'_'+str(curr.hour)+'_'+str(curr.minute)+'_'+str(curr.second)+".pdf"

def pdf_into_all(filename,path):
    if not os.path.isfile(filename):
         messagebox.showwarning("showwarning", "Given file can not be distributed (May be does not exist)!")
    input_pdf = PdfFileReader(filename)
    totalpages = input_pdf.numPages
    fname = filename.split('/').pop()
    for s in range(totalpages):
        output = PdfFileWriter()
        output.addPage(input_pdf.getPage(s))
        pagename=create_file_name(path+'/'+str(s+1)+'_page_'+fname)
        with open(pagename, "wb") as output_stream:
            output.write(output_stream)

def pdf_into_two(even,filename,path):
    if not os.path.isfile(filename):
         messagebox.showwarning("showwarning", "Given file can not be distributed (May be does not exist)!")
    input_pdf = PdfFileReader(filename)
    totalpages = input_pdf.numPages
    fname = filename.split('/').pop()
    for s in range(totalpages):
        if even:
            if (s+1) % 2 == 0:
                output = PdfFileWriter()
                output.addPage(input_pdf.getPage(s))
                pagename=create_file_name(path+'/'+str(s+1)+'_page_'+fname)
                with open(pagename, "wb") as output_stream:
                    output.write(output_stream)
        else:
            if (s+1) % 2 != 0:
                output = PdfFileWriter()
                output.addPage(input_pdf.getPage(s))
                pagename=create_file_name(path+'/'+str(s+1)+'_page_'+fname)
                with open(pagename, "wb") as output_stream:
                    output.write(output_stream)

def pdf_into_custom(filename,path):
    def pdf_custom():
        data = []
        for cell in table.entries:
            try:
                if len(cell[0].get()) == 0 and len(cell[1].get())==0:
                    continue
                if len(cell[0].get()) != 0 and len(cell[1].get()) == 0:
                    fst = int(cell[0].get())
                    snd=fst
                    data.append((fst,fst))
                if len(cell[1].get()) != 0 and len(cell[0].get()) == 0:
                    snd = int(cell[1].get())
                    fst=snd
                    data.append((snd,snd))
                if len(cell[1].get()) != 0 and len(cell[0].get()) != 0:
                    fst = int(cell[0].get())
                    snd = int(cell[1].get())
                    data.append((fst,snd))
                if snd > totalpages or fst > totalpages or snd < 1 or fst < 1 or fst > snd:
                    raise Exception()
            except ValueError:
                messagebox.showwarning("showwarning", "Only numbers are allowed!")
                return
            except Exception as ex:
                messagebox.showwarning("showwarning", "Numbers must be between 1 and the file size in pages ("+str(totalpages)+")!\nFirst number must be lesst than or equal as the second number!")
                return 
        for value in data:
            input_pdf = PdfFileReader(filename)
            fname = filename.split('/').pop()
            interval = ""
            if(value[0] == value[1]):
                interval = str(value[0])
            else:
                interval = str(value[0])+'_'+str(value[1])
            output = PdfFileWriter()
            for s in range((value[0]-1),value[1]):
                output.addPage(input_pdf.getPage(s))
            pagename=create_file_name(path+'/'+interval+'_page_'+fname)
            with open(pagename, "wb") as output_stream:
                output.write(output_stream)
        messagebox.showinfo("showinfo","The task is completed!")
        custom.destroy()
    
    if not os.path.isfile(filename):
        messagebox.showwarning("showwarning", "Given file can not be distributed (May be does not exist)!")
        return
    custom = Toplevel()
    custom.resizable(False,False)
    custom.geometry("400x300")
    custom.title("Costum Range - PDF Manipulator")
    custom.grab_set()
    input_pdf = PdfFileReader(filename)
    totalpages = input_pdf.numPages
    lbl1= Label(custom,text="No. of first page")
    lbl1.grid(row=0,column=0)
    lbl2= Label(custom,text="No. of last page")
    lbl2.grid(row=0,column=1)
    table = Table(custom)
    btn_ok = Button(custom,text="OK", command=pdf_custom, width=20, fg='#FFFFFF', bg="#FF0000")
    btn_ok.grid(row=13,column=0, columnspan=2, pady=30)
    custom.mainloop()

def add_pdf():
    filenames = filedialog.askopenfilenames(initialdir="/", title="Select Files", filetypes=(("PDF Files","*.pdf"),("All Files","*.*")))
    bad_ones = ""
    global i
    prev = i
    for x in filenames:
        if x[-4::] ==".pdf":
            ui.insert(i,str(x).strip())
            i = i+1
        else:
            bad_ones+=str(x).strip()+'\n'
    if len(bad_ones) > 0:
        messagebox.showwarning("showwarning", "Could not import the following(s) because they are not the right file type:\n"+bad_ones)
    if prev != i:
        global saved
        saved=False
        animate_menu()
    

def create_file():
    global i
    if i < 1:
        messagebox.showwarning("showwarning", "You must have at least one file in the session!")
        return
    def open_folder():
        dir = filedialog.askdirectory(initialdir="/", title="Select Directory")
        win_create.lift()
        txt2.delete("1.0","end")
        txt2.insert("1.0",dir)
    
    def c_a_m_file():
        name=''
        path=''
        name = str.strip(txt.get("1.0","end"))
        path= str.strip(txt2.get("1.0","end"))
        if name=='':
            messagebox.showwarning("showwarning", "You must give a name for the new file!")
            return
        if path=='':
            messagebox.showwarning("showwarning", "You must give a path for the new file!")
            return
        if not os.path.isdir(path):
            messagebox.showwarning("showwarning", "Invalid path!")
            return
        if not '.pdf' in name :
            name += '.pdf'
        path = path + '/' + name
        if(os.path.isfile(path)):
            answ=messagebox.askyesno("askyesno", "A file with this name already exists. If you continue the new will override the old one. Will you continue?")
            if not answ:
                return
        merge_pdfs(path)
        win_create.destroy()
        messagebox.showinfo("showinfo","The task is completed!")

    win_create=Toplevel(root)
    win_create.geometry("430x130")
    win_create.resizable(False,False)
    win_create.title('PDF Manipulator - Create New PDF File')
    win_create.grab_set()
    win_create.columnconfigure(1,weight=8)
    win_create.columnconfigure(2,weight=8)
    win_create.columnconfigure(3,weight=1)
    lbl=Label(win_create, text="New File Name")
    lbl.grid(row=1,column=1,pady=2)
    txt=Text(win_create,height=1,width=30)
    txt.grid(row=1,column=2,pady=2)
    lbl2=Label(win_create, text="New File Location")
    lbl2.grid(row=2,column=1,pady=2)
    txt2=Text(win_create,height=1,width=30)
    txt2.grid(row=2,column=2,pady=2)
    btn_ok=Button(win_create, text="OK",height=1,width=10,command=c_a_m_file, bg="#FF0000", fg="#FFFFFF")
    btn_ok.grid(row=3,column=1, columnspan=3,pady=25)
    img = Image.open('folder.png')
    img = img.resize((20,20))
    img = ImageTk.PhotoImage(img)
    btn_folder=Button(win_create, image=img,height=15,width=20, command=open_folder)
    btn_folder.grid(row=2,column=3,padx=20)
    win_create.mainloop()

def distribute_file():
    def open_fold():
        dir = filedialog.askdirectory(initialdir="/", title="Select Directory")
        win_dist.lift()
        txt2.delete("1.0","end")
        txt2.insert("1.0",dir)
        global path
        path = dir
    
    def c_a_m_file():
        path=''
        path= str.strip(txt2.get("1.0","end"))
        if path=='':
            messagebox.showwarning("showwarning", "You must give a path for the new file!")
            return False
        if not os.path.isdir(path):
            messagebox.showwarning("showwarning", "Invalid path!")
            return False
        global filename
        return True

    def switch():
        global filename
        ok = c_a_m_file()
        if not ok:
            return
        nonlocal var
        if var.get() == 1:
            win_dist.destroy()
            pdf_into_all(filename,path)
            messagebox.showinfo("showinfo","The task is completed!")
        elif var.get() == 2:
            win_dist.destroy()
            pdf_into_two(False,filename,path)
            messagebox.showinfo("showinfo","The task is completed!")
        elif var.get() == 3:
            win_dist.destroy()
            pdf_into_two(True,filename,path)
            messagebox.showinfo("showinfo","The task is completed!")
        elif var.get() == 4:
            win_dist.destroy()
            pdf_into_custom(filename,path)
        else:
            messagebox.showwarning("showwarning", "You must choose from the options!")
            return 
           
            

    if not ui.curselection() and i != 1:
        messagebox.showwarning("showwarning", "If you want to distribute a file you must have only one in the current session, or select one from the current session!")
        return
    global filename
    if ui.curselection():
        filename=str(ui.get(ACTIVE)).strip()
    else:
        filename=str(ui.get(0)).strip()
    if not os.path.isfile(filename):
        messagebox.showwarning("showwarning", "Can not distribute file (maybe it does not exist)!\n"+filename)
        return
    win_dist = Toplevel()
    win_dist.geometry("600x300")
    win_dist.resizable(False,False)
    win_dist.title('PDF Manipulator - Distribute PDF File')
    win_dist.grab_set()
    win_dist.columnconfigure(0, weight=1)
    win_dist.columnconfigure(1, weight=10)
    win_dist.columnconfigure(2, weight=5)
    lbl = Label(win_dist,text="File: "+filename, wraplength= 550,justify=LEFT, relief=RAISED, bg='#FF0000', fg="#FFFFFF", font=('Helvetica',10,'bold'))
    lbl.grid(row=0,column=0,columnspan=3,pady=2)
    var = IntVar()
    r1 = Radiobutton(win_dist,text="Distribute: Every page", variable=var, value=1)
    r1.grid(row=1,column=0,sticky="W",pady=1,columnspan=3)
    r2 = Radiobutton(win_dist,text="Distribute: Only odd page(s)", variable=var, value=2)
    r2.grid(row=2,column=0,sticky="W",pady=1,columnspan=3)
    r3 = Radiobutton(win_dist,text="Distribute: Only even page(s)", variable=var, value=3)
    r3.grid(row=3,column=0,sticky="W",pady=1,columnspan=3)
    r4 = Radiobutton(win_dist,text="Distribute: Costumized range", variable=var, value=4)
    r4.grid(row=4,column=0,sticky="W",pady=1,columnspan=3)
    btn_ok = Button(win_dist,text="OK", width=30, bg='#FF0000', fg="#FFFFFF", command=switch)
    btn_ok.grid(row=6,column=0,columnspan=3,pady=5)
    lbl2=Label(win_dist, text="New File Location")
    lbl2.grid(row=5,column=0,pady=5,sticky="E",padx=10)
    txt2=Text(win_dist,height=1,width=30)
    txt2.grid(row=5,column=1,pady=5,sticky="EW")
    img = Image.open('folder.png')
    img = img.resize((20,20))
    img = ImageTk.PhotoImage(img)
    btn_fold=Button(win_dist, image=img, height=15, width=20, command=open_fold)
    btn_fold.grid(row=5,column=2,pady=5,sticky="W",padx=10)
    win_dist.mainloop()
    
def save():
    file=open('session.txt','w+')
    data = ui.get(0,END)
    for i in data:
       file.write(str(i).strip()+'\n')
    file.close()
    global saved
    saved = True
    animate_menu()

def open_file():
    if len(ui.curselection()) < 1:
        messagebox.showwarning("showwarning", "First you must choose of file to open it!")
        return
    if not os.path.isfile(str(ui.get(ACTIVE)).strip()):
        messagebox.showwarning("showwarning", "Unable to open the file!\n"+str(ui.get(ACTIVE)))
    os.startfile(str(ui.get(ACTIVE)).strip())

def closing():
    global saved
    if not saved:
        if not messagebox.askyesno("askyesno", "Would you like to continue?\nIf you continue without saving the session the progression will be lost!"):
            return 
    root.destroy()

def read_from_file():
    global ui
    global i
    if(os.path.isfile('session.txt')):
        file = open('session.txt','r')
        data = file.readlines()
        file.close()
        problems = ""
        for s in data:
            if not s=="":
                curr = str(s).strip()
                if os.path.isfile(curr):
                    ui.insert(i,curr)
                    i = i+1
                else:
                    curr+='\n'
                    problems+=curr
        if len(problems) > 0:
            messagebox.showwarning("showwarning", "Could not import the followings because of various reasons:\n"+problems)
            global saved
            saved = False
            animate_menu()
        

def animate_menu():
    global saved
    if saved:
        menubar.entryconfigure(1,label="Save Session")
    else:
        menubar.entryconfigure(1,label="Save Session*")

def delete():
    def copy_content():
        counter = 0
        for s in ui.get(0,END):
            lb.insert(counter,s)
            counter = counter + 1
    
    def delete_items():
        if not messagebox.askyesno("askyesno", "Are you sure you want to remove the item(s)?"):
            return
        selection = lb.curselection()
        for item in selection[::-1]:
            lb.delete(item)
    
    def exit_procedure():
        global i
        global saved
        counter = 0
        ui.delete(0,END)
        for item in lb.get(0,END):
            ui.insert(counter,str(item).strip())
            counter = counter + 1
        if i != counter:
            saved = False
            animate_menu()
        i = counter
        win_del.destroy()
    
    win_del=Toplevel()
    win_del.geometry('600x400')
    win_del.title('PDF Manipulator - Remove Multiple Items')
    win_del.resizable(False,False)
    win_del.grab_set()
    win_del.protocol("WM_DELETE_WINDOW",exit_procedure)
    sb2 = Scrollbar(win_del, orient=HORIZONTAL)
    sb2.pack(side=BOTTOM, fill=X)
    lb = Listbox(win_del, selectmode=EXTENDED,selectbackground="red", selectforeground="white", font=("Helvetica",9,"bold"))
    lb.pack(side=LEFT,fill=BOTH, expand=True)
    sb = Scrollbar(win_del, orient=VERTICAL)
    sb.pack(side=RIGHT, fill=Y)
    lb.config(yscrollcommand=sb.set)
    sb.config(command=lb.yview)
    lb.config(xscrollcommand=sb2.set)
    sb2.config(command=lb.xview)
    img2 = PhotoImage(file="delete.png")
    btn_del = Button(win_del,image = img2,command=delete_items)
    btn_del.pack(side=RIGHT)
    copy_content()
    win_del.mainloop()

def popup_show(event):
    if ui.curselection():
        try:
            popup2.tk_popup(event.x_root, event.y_root, 0)
        finally:
            popup2.grab_release()
    else:
        try:
            popup1.tk_popup(event.x_root, event.y_root, 0)
        finally:
            popup1.grab_release()

def remove():
    global i
    for s in ui.curselection():
        ui.delete(s)
        i = i-1
    global saved
    saved=False
    animate_menu()

def remove_all():
    if not messagebox.askyesno("askyesno", "Are you sure you want to clear workspace?"):
        return
    ui.delete(0,END)
    global saved
    saved = False
    animate_menu()
    global i
    i=0

root = Tk()
root.geometry("715x394")
root.resizable(False,False)
icon = PhotoImage(file="./icon.png")
root.iconphoto(True,icon)
root.title('PDF Manipulator')


menubar=Menu(root)
menubar.add_command(label="Save Session", command=save)
menubar.add_command(label="Add Files", command=add_pdf)
menubar.add_command(label="Remove", command=delete)
menubar.add_command(label="Open", command=open_file)
menubar.add_command(label="Create PDF", command=create_file)
menubar.add_command(label="Distribute PDF", command=distribute_file)

popup2 = Menu(root,tearoff=0)

popup2.add_command(label="Remove Item", command=remove)
popup2.add_command(label="Clear Workspace", command=remove_all)

popup1 = Menu(root,tearoff=0)

popup1.add_command(label="Clear Workspace",command=remove_all)

root.config(menu=menubar)
root.protocol("WM_DELETE_WINDOW", closing)

sb2 = Scrollbar(root, orient=HORIZONTAL)
sb2.pack(side=BOTTOM, fill=X)

ui=DragDropListbox(root)
ui.bind("<Button-3>", popup_show)
ui.pack(side=LEFT,fill=BOTH, expand=True)

sb = Scrollbar(root, orient=VERTICAL)
sb.pack(side=RIGHT, fill=Y)

ui.config(yscrollcommand=sb.set)
sb.config(command=ui.yview)

ui.config(xscrollcommand=sb2.set)
sb2.config(command=ui.xview)

read_from_file()

root.mainloop()